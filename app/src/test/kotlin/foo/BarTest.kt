package foo

import org.junit.Assert.assertTrue
import org.junit.Test

class BarTest {
    @Test
    fun shouldCreateBar() {
        val bar = Bar()
        assertTrue(bar.s == "test")
    }
}
